import gql from "graphql-tag";

const GET_POSTS = gql` query
 {
    posts{
        postId 
        postText 
        title 
        postType
        createDate
    }
   
  }
`;

const GET_POST = gql` 
    query getPost($postId: Int!) {
        post(postId: $postId) {
            postId 
            postText 
            title 
            postType 
            createDate
        }
    }
`;

const ADD_POST = gql`
    mutation addPost($title: String!, $postText: String!, $postType: String!, $createDate: String!){
        addPost(title: $title, postText: $postText, postType: $postType, createDate: $createDate){
            postId
        }
    }
    
`;


export {GET_POSTS, GET_POST, ADD_POST}