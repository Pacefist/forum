import React from "react";
import {Card} from "semantic-ui-react";
import {Link} from "react-router-dom";
import * as moment from "moment";

const PostCard = ({post}) => {
    return  (<Card >
        <Card.Content>
            <Card.Header>
                <Link to={`/posts/${post.postId}`}>{post.title}</Link>
            </Card.Header>
            <Card.Meta>
                created {moment(post.createDate).format('YYYY MM DD h:mm:ss')}
            </Card.Meta>
            <Card.Description>
                <div dangerouslySetInnerHTML={{__html: post.postText}}></div>
            </Card.Description>
        </Card.Content>
    </Card>)
};

export default PostCard;