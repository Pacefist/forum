import React, {Component} from "react";
import {Editor} from 'react-draft-wysiwyg';
import {EditorState, convertToRaw} from 'draft-js';
import draftToHtml from "draftjs-to-html";
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';


/* I don not use
ref={node => {
    postTitle = node;
}}

because I use editor.. and for this i need change detection.
 */

class PostForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            editorState: EditorState.createEmpty(),
            title: '',
        };
        this.onEditorStateChange = this.onEditorStateChange.bind(this);
        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }
    onEditorStateChange(editorState) {
        this.setState({editorState});
    };

    onChangeTitle(event) {
        this.setState({title: event.target.value});
    };

    handleSubmit(event) {
        event.preventDefault();
        const textFromEditor = this.state.editorState.getCurrentContent();
        const postData = {
            title: this.state.title,
            postText: this.formattingText(textFromEditor),
            postType: 'note',
            createDate: new Date()
        };

        this.props.onSubmit(postData);
    };

    formattingText(value) {
        return draftToHtml(convertToRaw(value))
    }

    render() {
        const {editorState} = this.state;
        return (
            <form onSubmit={this.handleSubmit}>
                <input type="text" name="name" placeholder="title" onChange={this.onChangeTitle}
                       value={this.state.title}/>
                <Editor
                    editorState={editorState}
                    toolbarClassName="toolbarClassName"
                    wrapperClassName="wrapperClassName"
                    editorClassName="editorClassName"
                    onEditorStateChange={this.onEditorStateChange}
                />
                <input type="submit" value="Submit"/>
            </form>
        )
    }
}

export default PostForm;