import React, {Component} from 'react';
import {Query} from 'react-apollo';
import {Card} from 'semantic-ui-react';

import Loading from "../../shared/loading";
import Error from "../../shared/error";
import {GET_POSTS} from '../../queries/post';

import PostCard from './components/postCard'

import './posts.css';

export default class Posts extends Component {
    render() {
        return (
            <Query query={GET_POSTS}>
                {({loading, error, data: {posts}}) => {
                    if (loading) {
                        return <Loading/>
                    }

                    if (error) {
                        return <Error error={error.message}/>
                    }

                    return (
                        <div>
                            <Card.Group itemsPerRow={1}>
                                {posts.map((post, key) => <PostCard key={key} post={post}></PostCard>)}
                            </Card.Group>
                        </div>
                    )
                }}
            </Query>
        );
    }
}