import React, {Component} from 'react';
import * as moment from "moment/moment";
import {Item} from 'semantic-ui-react';

import {Query} from "react-apollo";
import {Card} from "semantic-ui-react/dist/commonjs/views/Card/Card";

import {GET_POST} from './../../queries/post';
import Loading from "../../shared/loading";
import Error from "../../shared/error";


export default class PostSingle extends Component {
    render() {
        const {postId} = this.props.match.params;
        return (
            <Query query={GET_POST} variables={{postId}}>
                {({loading, error, data: {post}}) => {
                    if (loading) {
                        return <Loading/>
                    }

                    if (error) {
                        return <Error error={error.message}/>
                    }
                    return (
                        <Item.Group>
                            <Item>
                                <Item.Content>
                                    <Item.Header as='a'>{post.title}</Item.Header>
                                    <Item.Meta>created {moment(post.createDate).format('YYYY MM DD h:mm:ss')}</Item.Meta>
                                    <Item.Description>
                                        <div dangerouslySetInnerHTML={{__html: post.postText}}></div>
                                    </Item.Description>
                                </Item.Content>
                            </Item>
                        </Item.Group>
                    )
                }}
            </Query>

        )
    }
}