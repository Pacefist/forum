
import {Mutation, Query} from 'react-apollo'
import React from 'react'
import * as R from 'ramda'
import {adopt} from 'react-adopt'

import normalizeIncomeCategories from 'modules/addTransactionWidget/form/normalizers/normalizeIncomeCategories'
import normalizeExpenseCategories from 'modules/addTransactionWidget/form/normalizers/normalizeExpenseCategories'
import normalizeAccounts from 'modules/addTransactionWidget/form/normalizers/normalizeAccounts'
import normalizeUserCurrencies from 'modules/addTransactionWidget/form/normalizers/normalizeUserCurrencies'
import transactionsQuery from 'graphql/queries/transactions'
import updateTransactionMutation from 'graphql/mutations/updateTransaction'
import FormikForm from 'modules/addTransactionWidget/form/formikForm'
import expenseCategoriesQuery from 'graphql/queries/expenseCategories'
import incomeCategoriesQuery from 'graphql/queries/incomeCategories'
import userCurrenciesQuery from 'graphql/queries/userCurrencies'
import accountsQuery from 'graphql/queries/accounts'
import addTransactionMutation from 'graphql/mutations/addTransaction'
import addTransactionWidgetQuery from 'graphql/queries/addTransactionWidget'

const updateTransaction = ({render}) => (
    <Mutation
        mutation={updateTransactionMutation}
        refetchQueries={() => [
            {query: accountsQuery},
            {query: transactionsQuery, variables: {startDate: '2018-08-24', endDate: '2018-08-24'}}
        ]}
    >
        {(mutation, result) => render({mutation, result})}
    </Mutation>
)

const addTransaction = ({render}) => (
    <Mutation
        mutation={addTransactionMutation}
        refetchQueries={() => [{query: accountsQuery}]}
        update={(cache, {data: {createTransaction}}) => {
            const variables = {startDate: '2018-08-24', endDate: '2018-08-24'}
            const {transactions} = cache.readQuery({
                query: transactionsQuery,
                variables
            })
            cache.writeQuery({
                query: transactionsQuery,
                variables,
                data: {
                    transactions: R.prepend(createTransaction, transactions)
                }
            })
        }}
    >
        {(mutation, result) => render({mutation, result})}
    </Mutation>
)

const WithGraphQL = adopt({
    expenseCategoriesResponse: <Query query={expenseCategoriesQuery} />,
    incomeCategoriesResponse: <Query query={incomeCategoriesQuery} />,
    userCurrenciesResponse: <Query query={userCurrenciesQuery} />,
    accountsResponse: <Query query={accountsQuery} />,
    addTransactionWidgetResponse: <Query query={addTransactionWidgetQuery}/>,
    updateTransactionResponse: updateTransaction,
    addTransactionResponse: addTransaction
})

export default ({startDate, endDate, editingTransaction}) => (
    <WithGraphQL>
        {({
              expenseCategoriesResponse,
              incomeCategoriesResponse,
              userCurrenciesResponse,
              accountsResponse,
              addTransactionWidgetResponse,
              updateTransactionResponse,
              addTransactionResponse
          }) => {
            console.log('updateTransaction', updateTransaction)
            const expenseCategories = R.compose(
                normalizeExpenseCategories,
                R.pathOr([], ['data', 'expenseCategories'])
            )(expenseCategoriesResponse)
            const incomeCategories = R.compose(
                normalizeIncomeCategories,
                R.pathOr([], ['data', 'incomeCategories'])
            )(incomeCategoriesResponse)
            const currencies = R.compose(
                normalizeUserCurrencies,
                R.pathOr([], ['data', 'userCurrencies'])
            )(userCurrenciesResponse)
            const accounts = R.compose(
                normalizeAccounts,
                R.pathOr([], ['data', 'accounts'])
            )(accountsResponse)
            const transactionType = R.compose(
                R.pathOr([], ['data', 'addTransactionWidget', 'transactionType'])
            )(addTransactionWidgetResponse)

            return (
                <FormikForm
                    addTransaction={addTransactionResponse.mutation}
                    updateTransaction={updateTransactionResponse.mutation}
                    transactionType={transactionType}
                    expenseCategories={expenseCategories}
                    incomeCategories={incomeCategories}
                    currencies={currencies}
                    accounts={accounts}
                    editingTransaction={editingTransaction}
                    date={startDate}
                />
            )
        }}
    </WithGraphQL>
)

// export default ({startDate, endDate, editingTransaction}) => (
//   <Mutation
//     mutation={UPDATE_TRANSACTION}
//     refetchQueries={() => [
//       {query: ACCOUNTS_QUERY},
//       {query: TRANSACTIONS_QUERY, variables: {startDate, endDate}}
//     ]}
//   >
//     {updateTransaction => (
//       <Mutation
//         mutation={ADD_TRANSACTION}
//         refetchQueries={() => [{query: ACCOUNTS_QUERY}]}
//         update={(cache, {data: {createTransaction}}) => {
//           console.log('after create transaction')
//           const variables = {
//             startDate,
//             endDate
//           }
//           const {transactions} = cache.readQuery({
//             query: TRANSACTIONS_QUERY,
//             variables
//           })
//           cache.writeQuery({
//             query: TRANSACTIONS_QUERY,
//             variables,
//             data: {
//               transactions: R.prepend(createTransaction, transactions)
//             }
//           })
//         }}
//       >
//         {addTransaction => (
//           <ExpenseCategoriesQuery>
//             {({expenseCategories}) => (
//               <IncomeCategoriesQuery>
//                 {({incomeCategories}) => (
//                   <AccountsQuery>
//                     {({accounts}) => (
//                       <UserCurrenciesQuery>
//                         {({userCurrencies}) => (
//                           <AddTransactionWidgetQuery>
//                             {({addTransactionWidget}) => (
//                               <FormikForm
//                                 addTransaction={addTransaction}
//                                 updateTransaction={updateTransaction}
//                                 transactionType={addTransactionWidget.transactionType}
//                                 expenseCategories={normalizeExpenseCategories(expenseCategories)}
//                                 incomeCategories={normalizeIncomeCategories(incomeCategories)}
//                                 currencies={normalizeUserCurrencies(userCurrencies)}
//                                 accounts={normalizeAccounts(accounts)}
//                                 editingTransaction={editingTransaction}
//                                 date={startDate}
//                               />
//                             )}
//                           </AddTransactionWidgetQuery>
//                         )}
//                       </UserCurrenciesQuery>
//                     )}
//                   </AccountsQuery>
//                 )}
//               </IncomeCategoriesQuery>
//             )}
//           </ExpenseCategoriesQuery>
//         )}
//       </Mutation>
//     )}
//   </Mutation>
// )