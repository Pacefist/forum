import React, {Component} from 'react';
import {Mutation} from 'react-apollo';

import Loading from '../../shared/loading';
import Error from '../../shared/error';
import PostForm from './components/postForm';

import {ADD_POST} from './../../queries/post';

export default class PostCreate extends Component {

    goToCreatedPost(value) {
        this.props.history.push(`/posts/${value}`);
    };

    render() {
        return (
            <Mutation mutation={ADD_POST}>
                {(addPost, {loading, error}) => {
                    if (loading) {
                        return <Loading/>
                    }

                    if (error) {
                        return <Error error={error.message}/>
                    }

                    return (
                        <PostForm
                            onSubmit={postData => {
                                addPost({variables: postData}).then(res => {
                                    this.goToCreatedPost(res.data.addPost.postId);
                                });

                            }}
                        />
                    )
                }}
            </Mutation>
        )
    }
}