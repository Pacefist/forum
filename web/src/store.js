import { combineReducers, createStore, compose, applyMiddleware } from 'redux';
import createLogger from 'redux-logger';
import apolloClient from './apolloClient';
import loginReducer from './reducers/login';
import userReducer from './reducers/user';

const reducer = combineReducers({
    apollo: apolloClient.watchQuery,
    login: loginReducer,
    user: userReducer
});

// const store = createStore(reducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export default createStore(reducer, {},
    compose(
        applyMiddleware(apolloClient.middleware(), createLogger()),
        window.devToolsExtension ? window.devToolsExtension() : f => f,
    )
)