import React  from 'react';

import ApplicationModel from "./models/applicaiton";
import Header from "./shared/header/header";
import SubMenu from "./shared/submenu/submenu";
import Footer from "./shared/footer/footer";
import {Redirect, Route, Switch} from "react-router-dom";
import PostCreate from "./pages/posts/post-create.componemt";
import Post from "./pages/posts/post.componemt";
import Posts from "./pages/posts/posts.componemt";

const Container = ({children}) => (
    <div className="app-content">
        <div className="app-container">{children}</div>
    </div>
);

const HomePage = () => (
    <div>
        <div>HomePage</div>
        <Redirect to={{pathname: "/posts"}}/>
    </div>
);

const App = () => (
    <div className="app-page">
        <Header menu={ApplicationModel.header.menu}/>
        <SubMenu menu={ApplicationModel.header.subMenu}/>
        <Container>
            <Switch>
                <Route exact path='/' component={HomePage}/>
                <Route exact path='/posts/new' component={PostCreate}/>
                <Route path='/posts/:postId' component={Post}/>
                <Route path='/posts' component={Posts}/>
            </Switch>
        </Container>
        <Footer/>
    </div>
);

export default App;