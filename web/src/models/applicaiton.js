const ApplicationModel = {

    header: {
        menu: [
            {
                title: 'Blog',
                path: '/',
            }
        ],
        subMenu: [
            {
                title: 'Create post',
                path: '/posts/new',
            }
        ]
    }
};


export default ApplicationModel;