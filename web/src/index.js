import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import {ApolloProvider} from 'react-apollo';

import 'semantic-ui-css/semantic.min.css';
import './index.css';

import App from './App';
import registerServiceWorker from './registerServiceWorker';
import apolloClient from "./apolloClient";

ReactDOM.render(
    <ApolloProvider client={apolloClient}>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
    </ApolloProvider>,
    document.getElementById('root')
);
registerServiceWorker();
