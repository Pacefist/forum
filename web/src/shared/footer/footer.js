import React from 'react';
import './footer.css'
const Footer = () => {
    return <div className="app-footer">
        <div className="app-container page-container">
            develop by Liubomyr
        </div>
    </div>
};

export default Footer;