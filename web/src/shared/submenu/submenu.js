import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

import './submenu.css'


class SubMenu extends Component {
    static propTypes = {
        menu: PropTypes.arrayOf(
            PropTypes.shape({
                title: PropTypes.string,
                path: PropTypes.string
            })
        )
    };

    render() {
        return (
            <div className="app-submenu">
                <div className="app-container">
                    <div className="menu">
                        {this.props.menu.map((link, index) => {
                            return (<span className="menu-item" key={index}><Link className="menu-link"
                                                                                  to={link.path}>{link.title}</Link></span>)
                        })}
                    </div>
                </div>
            </div>
        )
    }
}


export default SubMenu;