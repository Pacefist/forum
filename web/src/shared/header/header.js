import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

import './header.css'
// import FacebookLoginProvider from "../../core/providers/facebookLogin";


export default class Header extends Component {
    constructor(props) {
        super(props)

        this.onClickLogin.bind(this);
    }

    static propTypes = {
        menu: PropTypes.arrayOf(
            PropTypes.shape({
                title: PropTypes.string,
                path: PropTypes.string
            })
        )
    };

    onClickLogin(e) {
        e.preventDefault();
        debugger
        // authService.login();
    }

    render() {
        return (
            <div className="app-header">
                <div className="app-container">
                    <div className="menu">
                        {this.props.menu.map((link, index) => {
                            return (<span className="menu-item" key={index}><Link className="menu-link"
                                                                                  to={link.path}>{link.title}</Link></span>)
                        })}
                    </div>
                    <div className="sub-menu">
                        <span className="menu-item">
                            {/*<FacebookLoginProvider>{api => (*/}
                                {/*<a onClick={api.login} className="menu-link">login</a>*/}
                            {/*)}</FacebookLoginProvider>*/}
                        </span>
                    </div>
                </div>
            </div>
        )
    }
}