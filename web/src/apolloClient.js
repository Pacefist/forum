import ApolloClient from 'apollo-client';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {ApolloLink} from "apollo-link";
import {createHttpLink} from "apollo-link-http";

const httpLink = createHttpLink({
    uri: "http://127.0.0.1:5000/graphql"
});

// const middlewareLink = new ApolloLink((operation, forward) => {
//     operation.setContext({
//         headers: {
//             authorization: localStorage.getItem("token") || null
//         }
//     });
//     return forward(operation);
// });

const cache = new InMemoryCache();
// const httpLinkWithAuthToken = middlewareLink.concat(httpLink);

const apolloClient = new ApolloClient({
    link:  ApolloLink.from([httpLink]),
    cache
});

export default apolloClient